def gitclone(String branches,String credentialsId,String url)
{
    
    git branch: ${branches}, credentialsId: ${credentialsId}, url: ${url}
        
}

def maven()
{
    def mavenHome = tool name: "maven"
     sh "${mavenHome}/bin/mvn clean package"
}

def artifact()
{
     sh "aws s3 cp /home/pranay/slave2/workspace/shared-new/target/maven-web-application.war s3://pipeline-artifact-image/Artifact/maven-web-application.war"
    
}
def buildimageslave2(String project, String hubUser) {
    sh "docker image build -t ${hubUser}/${project}:beta-${env.BRANCH_NAME}-${env.BUILD_NUMBER} ."
    
    
    
    //sh "docker  push ${hubUser}/${project}:beta-${env.BRANCH_NAME}-${env.BUILD_NUMBER}"
}

def imagetoaws()
{
    sh "docker images"
    sh "docker save -o maven.tar kuntpranay9949/maven:latest"
    sh "aws s3 cp  /home/pranay/slave2/workspace/pipeline-2/maven.tar s3://pipeline-artifact-image/images/maven.tar"
				
}
